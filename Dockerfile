FROM node:12.13.0-alpine AS production
# Docker build -t api-notification:1.0.0 .

ARG WORK_DIR=/var/www/node
RUN mkdir -p ${WORK_DIR}
COPY . ${WORK_DIR}/
WORKDIR ${WORK_DIR}

RUN apk update && apk upgrade && apk add --no-cache bash git
RUN npm install
RUN npm run prod:build
RUN rm -fr node_modules
RUN rm -fr src
RUN npm install --production
RUN rm -f .npmrc

ENTRYPOINT  ["npm", "run", "start:prod"]
