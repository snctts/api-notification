import {NestFactory} from '@nestjs/core';
import {promisify} from 'util';

jest.setTimeout(60000);

const wait = promisify(setTimeout);

describe('main.ts', () => {
  const mockProvider = {
    log: jest.fn(),
    methodsAndControllerMethodsWithMetaAtKey: jest.fn().mockReturnValue([]),
    get: () => {
      return '';
    },
  };
  const mockUpdatePolicyFn = jest.fn();

  beforeAll(() => {
    jest
      .spyOn(NestFactory, 'create')
      .mockImplementation(async (module: any, settings: any): Promise<any> => {
        return {
          get: (providerKey: any) => mockProvider,
          use: (middleware: any) => {},
          useGlobalPipes: (pipe: any) => {},
          useGlobalFilters: (filter: any) => {},
          setGlobalPrefix: (prefix: string) => {},
          close: () => ({
            catch: (fn) => fn(),
          }),
          getHttpAdapter: () => ({
            get: (...args: any[]) => {},
            getType: () => 'express',
          }),
          listen: (port: number) => {},
          container: {
            getModules: () => new Set(),
          },
          enableCors: (...config) => {},
        };
      });
  });
  it('works!', async () => {
    import('./main');
    await wait(6000);
  });
});
