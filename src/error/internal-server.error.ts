import {BaseError} from './base.error';
import {HttpStatus} from '@nestjs/common';

export class InternalServerError extends BaseError {
  constructor(message, customStack = {}) {
    super(message, HttpStatus.INTERNAL_SERVER_ERROR, customStack);
  }
}
