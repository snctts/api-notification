import {BaseError} from './base.error';
import {HttpStatus} from '@nestjs/common';

describe('BaseError', () => {
  it('should provide appropriate methods when extending from it', () => {
    class TestError extends BaseError {
      constructor(message) {
        super(message, HttpStatus.NOT_FOUND);
      }
    }

    const errorInstance: TestError = new TestError('Test message');

    expect(errorInstance.getStatusCode()).toBe(404);

    expect(errorInstance.getObject()).toEqual({
      statusCode: 404,
      name: 'TestError',
      message: 'Test message',
      customStack: {},
      stackTrace: expect.any(String),
    });

    expect(errorInstance.getInnerException()).toEqual({});
  });
});
