import {HttpException} from '@nestjs/common';

export abstract class BaseError extends HttpException {
  public statusCode;
  public customStack;
  public customData;

  constructor(message: string | object, statusCode: number, customStack = {}, customData = null) {
    super(message, statusCode);

    this.name = this.constructor.name;
    this.statusCode = statusCode;
    this.customStack = customStack;
    this.customData = customData;
  }

  public getStatusCode() {
    return this.statusCode;
  }

  public getObject() {
    const errorObject: any = {
      statusCode: this.statusCode,
      name: this.name,
      message: this.message,
      customStack:
        this.customStack instanceof Error
          ? {
              innerData: this.customStack,
              innerStackTrace: this.customStack.stack,
            }
          : this.customStack,
    };

    errorObject.stackTrace = `${this.stack}`;

    return errorObject;
  }

  public getInnerException(): any {
    const errorObject = this.getObject();
    if (errorObject.customStack.innerData) {
      return errorObject.customStack.innerData;
    } else {
      return errorObject.customStack;
    }
  }
}
