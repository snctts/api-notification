import {INestApplication, ValidationPipe} from '@nestjs/common';
import {Test, TestingModuleBuilder} from '@nestjs/testing';
import {MongoMemoryReplSet, MongoMemoryServer} from 'mongodb-memory-server';
import * as responseTime from 'response-time';
import {AppModule} from '../app.module';
import * as supertest from 'supertest';

import * as dbHelper from './db';
import {AnyExceptionFilter} from '../filters/http-exception.filter';
import {GatewayHttpExceptionFilter} from '../filters/gateway-http-exception.filter';

export const createRequestFunction = (app: INestApplication) =>
  async function request(
    url: string,
    {
      expected = 200,
      method = 'get',
      body,
      contentType = 'application/json',
      accept = 'application/json',
      attachment,
      query,
    }: {
      expected?: number;
      method?: 'get' | 'post' | 'put' | 'delete';
      body?: any;
      contentType?: string;
      accept?: string;
      attachment?: {
        name: string;
        file: string;
      };
      query?: Record<string, any>;
    } = {},
  ) {
    const agent = supertest.agent(app.getHttpServer());
    const req = agent[method](url)
      .set('Accept', accept)
      .set('sub', 'sub')
      .set('access-token', 'mock-token');
    if (attachment) {
      req.attach(attachment.name, attachment.file);
    }
    if (query) {
      req.query(query);
    }
    const reqAfterSend = body ? req.set('Content-Type', contentType).send(body) : req;
    return reqAfterSend.expect(expected).then((res) => res);
  };

export const initTestApp = async (
  overrides?: (testModule: TestingModuleBuilder) => TestingModuleBuilder,
): Promise<[INestApplication, MongoMemoryServer]> => {
  const db = await MongoMemoryServer.create({
    instance: {
      replSet: 'rs-xx',
    },
  });
  const replSet = new MongoMemoryReplSet();
  await replSet.waitUntilRunning();

  const uri = await replSet.getUri();

  const dbUrl = `${uri}&retryWrites=false`;
  jest.spyOn(dbHelper, 'createMongooseOptions').mockImplementation(() => ({
    uri: dbUrl,
    useUnifiedTopology: true,
    useNewUrlParser: true,
    useCreateIndex: true,
    useFindAndModify: false,
  }));
  jest.spyOn(dbHelper, 'getNotificationMongoUri').mockReturnValue(dbUrl);

  let testBuilder = Test.createTestingModule({
    imports: [AppModule],
  });
  if (overrides) {
    testBuilder = overrides(testBuilder);
  }

  const fixture = await testBuilder.compile();

  const app = fixture.createNestApplication();

  app.use(responseTime({header: 'x-response-time'}));
  const adapterHost = app.getHttpAdapter();
  app.useGlobalPipes(new ValidationPipe({transform: true, whitelist: true}));
  app.useGlobalFilters(new AnyExceptionFilter(adapterHost));
  app.useGlobalFilters(new GatewayHttpExceptionFilter());

  await app.init();
  return [app, db];
};
