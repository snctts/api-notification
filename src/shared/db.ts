import {MongooseModuleOptions} from '@nestjs/mongoose';
import {getConfig} from '../common/config.provider';

export function createMongooseOptions(uriConfigPath: string): MongooseModuleOptions {
  return {
    uri: getConfig().get(uriConfigPath),
    useUnifiedTopology: true,
    useNewUrlParser: true,
    useCreateIndex: true,
    useFindAndModify: false,
  };
}

export function getNotificationMongoUri() {
  return getConfig().get('mongodb.notification.uri');
}
