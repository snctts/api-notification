import {SwaggerModule, DocumentBuilder} from '@nestjs/swagger';
import {INestApplication, ValidationPipe} from '@nestjs/common';
import {getConfig} from '../common/config.provider';
import {AnyExceptionFilter} from '../filters/http-exception.filter';
import {GatewayHttpExceptionFilter} from '../filters/gateway-http-exception.filter';
import * as config from 'config';
import {HttpAdapterHost} from '@nestjs/core';
const serviceInfo = getConfig().get('service');

export const configureApp = (app: INestApplication) => {
  app.enableCors({
    origin: '*',
    methods: 'GET,HEAD,POST,PUT,DELETE,PATCH',
    credentials: true,
  });
  app.setGlobalPrefix(config.get('service.baseUrl'));
  app.useGlobalPipes(new ValidationPipe({transform: true, whitelist: true}));
  const {httpAdapter} = app.get(HttpAdapterHost);
  app.useGlobalFilters(new AnyExceptionFilter(httpAdapter));
  app.useGlobalFilters(new GatewayHttpExceptionFilter());
  app.enableShutdownHooks();
};

export const initializeSwagger = async (app: INestApplication) => {
  const options = new DocumentBuilder()
    .setTitle('Savis Send Notifications API')
    .setDescription('API specification for Savis Send Notifications service ')
    .setVersion(serviceInfo.apiVersion)
    .addServer(`${config.get('server.swaggerSchema')}://${config.get('server.hostname')}`)
    .addBearerAuth({type: 'apiKey', in: 'header', name: 'access-token'})
    .build();

  const document = SwaggerModule.createDocument(app, options);

  SwaggerModule.setup(serviceInfo.docsBaseUrl, app, document, {
    swaggerOptions: {
      displayOperationId: true,
    },
  });
};
