import * as config from 'config';
import {v4 as uuidV4} from 'uuid';
import * as httpContext from 'express-http-context';
import * as pino from 'pino';
import {pinoConfig} from '../common/config.provider';

const env = process.env.NODE_ENV || 'development';
const serviceName = config.get('service.name');
export const logger = pino(pinoConfig);

enum LogLevel {
  Fatal,
  Error,
  Warning,
  Info,
  Debug,
}

const logLevel2Func = {
  [LogLevel.Error]: logger.error,
  [LogLevel.Warning]: logger.warn,
  [LogLevel.Info]: logger.info,
  [LogLevel.Debug]: logger.debug,
  [LogLevel.Fatal]: logger.fatal,
};

function log(
  logLevel: LogLevel,
  {methodName, data, customMessage}: {methodName: string; data?: any; customMessage?: string},
): void {
  const logFunc = logLevel2Func[logLevel];

  if (!logFunc) {
    logger.fatal({}, `No log func for level ${logLevel}`);
    return;
  }

  if (pinoConfig.enabled) {
    const correlationId = getCorrelationId();
    const processId = getProcessId();
    customMessage = customMessage || '';
    const message = `[Notification][${env}][${serviceName}][${correlationId}] ${methodName} : (${processId}) : ${customMessage}`;
    logFunc.call(
      logger,
      {data, level: logLevel, methodName, correlationId, userId: httpContext.get('userId')},
      message,
    );
  }
}

export function setCorrelationId(req, _res, next) {
  req.timestamp = Date.now();
  const correlationId = uuidV4();
  req.correlationId = correlationId;
  httpContext.set('correlationId', correlationId);
  next();
}

function getCorrelationId() {
  let correlationId = httpContext.get('correlationId');
  if (!correlationId) {
    correlationId = uuidV4();
    httpContext.set('correlationId', correlationId);
  }
  return correlationId;
}

function getProcessId() {
  let processId = httpContext.get('processId');
  if (!processId) {
    processId = uuidV4();
    processId = processId.replace(new RegExp('-', 'g'), '');
    httpContext.set('processId', processId);
  }
  return processId;
}

export function errorLog(methodName: string, data?: any, customMessage?: string): void {
  log(LogLevel.Error, {data, customMessage, methodName});
}

export function debugLog(methodName: string, data?: any, customMessage?: string): void {
  log(LogLevel.Debug, {data, customMessage, methodName});
}

export function infoLog(methodName: string, data?: any, customMessage?: string): void {
  log(LogLevel.Info, {data, customMessage, methodName});
}

export function warnLog(methodName: string, data: any, customMessage?: string): void {
  log(LogLevel.Warning, {data, customMessage, methodName});
}

export function fatalLog(methodName: string, data?: any, customMessage?: string): void {
  log(LogLevel.Fatal, {data, customMessage, methodName});
}
