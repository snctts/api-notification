export {};
declare global {
  namespace jest {
    interface Matchers<R> {
      toBeWithinMS(expected: Date, offset: number): R;
    }
  }
}
