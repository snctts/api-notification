import {Catch, ArgumentsHost, HttpException, InternalServerErrorException} from '@nestjs/common';
import {BaseExceptionFilter} from '@nestjs/core';
import {InternalServerError} from '../error/internal-server.error';
import {infoLog} from '../shared/logger';

@Catch()
export class AnyExceptionFilter extends BaseExceptionFilter {
  catch(exception: any, host: ArgumentsHost) {
    this.updateException(exception);
    super.catch(exception, host);
    this.trackException(exception, host);
  }

  trackException(exception: any, host: ArgumentsHost) {
    if (
      exception instanceof HttpException &&
      !(exception instanceof InternalServerError) &&
      !(exception instanceof InternalServerErrorException)
    ) {
      return;
    }

    const {query, params, body, user} = host.switchToHttp().getRequest();

    infoLog(JSON.stringify({query, body, params, userId: user && user.sub}));

    exception.stackTrace = exception.stack;
  }

  updateException(exception) {
    if (exception.response) {
      if (typeof exception.response === 'string') {
        const tempResponseString = exception.response;
        exception.response = {
          message: tempResponseString,
        };
      }

      if (!exception.response.statusCode) {
        exception.response.statusCode = exception.status;
      }

      if (!exception.response.message) {
        exception.response.message = exception.response.error;
      }

      if (!exception.response.name) {
        exception.response.name = exception.constructor.name;
      }

      if (exception.customData && Object.keys(exception.customData).length) {
        exception.response.customData = exception.customData;
      }
    }
  }
}
