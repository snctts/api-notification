import {AnyExceptionFilter} from './http-exception.filter';
import {
  Catch,
  ArgumentsHost,
  HttpException,
  InternalServerErrorException,
  BadRequestException,
} from '@nestjs/common';
describe('http-exception-filter', () => {
  it('should work it!', async () => {
    class TestAnyExceptionFilter extends AnyExceptionFilter {}

    const instanceExceptionFilter: TestAnyExceptionFilter = new TestAnyExceptionFilter();

    const error = new BadRequestException({
      response: 'test',
    });

    const host: ArgumentsHost = {
      switchToHttp: jest.fn().mockReturnValue({
        getRequest: jest.fn().mockReturnValue({
          query: 'query Test',
          body: 'body Test',
          params: 'params Test',
          user: 'user Test',
        }),
      }),
    } as any;

    expect(instanceExceptionFilter.updateException(error)).toEqual(undefined);
    expect(instanceExceptionFilter.trackException(error, host)).toEqual(undefined);
    expect(instanceExceptionFilter.trackException({stack: ''}, host)).toEqual(undefined);
  });
});
