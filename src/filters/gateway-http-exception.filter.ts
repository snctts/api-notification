import {ExceptionFilter, Catch, ArgumentsHost} from '@nestjs/common';
import {Response} from 'express';

@Catch()
export class GatewayHttpExceptionFilter implements ExceptionFilter {
  catch(exception: any, host: ArgumentsHost) {
    const ctx = host.switchToHttp();
    const response = ctx.getResponse<Response>();
    const caught = getResponse(exception);
    const statusCode = caught.statusCode || caught.status || exception.status || 500;

    response.status(statusCode).json({
      statusCode,
      message: caught.body?.message || caught.message,
      timestamp: new Date().toString(),
      errorResponse: caught.body?.error || caught.error,
      errorCode: caught.body?.errorCode || caught.errorCode,
      errors: caught.body?.errors || caught.errors,
      name: caught.body?.name || caught.name,
    });
  }
}

export function getResponse(exception: any): any {
  return exception.response ? getResponse(exception.response) : exception;
}
