import {Module} from '@nestjs/common';
import {MongooseModule} from '@nestjs/mongoose';
import {ScheduleModule} from '@nestjs/schedule';
import * as config from 'config';
import * as mongoose from 'mongoose';
import {CommonModule} from './common/config.module';
import {SendNotificationModule} from './modules/send-notification-firebase/notification-firebase.module';
import {createMongooseOptions} from './shared/db';

mongoose.set('debug', config.mongodb.debug);

@Module({
  imports: [
    ScheduleModule.forRoot(),
    SendNotificationModule,
    CommonModule,
    MongooseModule.forRootAsync({
      useFactory: () => createMongooseOptions('mongodb.notification.uri'),
    }),
  ],
  controllers: [],
  providers: [],
})
export class AppModule {}
