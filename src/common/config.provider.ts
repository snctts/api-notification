import {config as dotenvConfig} from 'dotenv';
dotenvConfig();
import {PinoConfig} from './config.interface';

import * as config from 'config';
import {IConfig} from 'config';

export const getHost = () => {
  const hostname = config.get('server.hostname');
  if (hostname) {
    return `${hostname}`;
  } else {
    return `${config.get('server.host')}:${config.get('server.port')}`;
  }
};

export const getConfigFirebase = () => {
  return config.get('firebase');
};

export const getPort = (): string => {
  return `${config.get('server.port')}`;
};

export const getConfig = () => {
  return config;
};

export const CONFIG_SERVICE_PROVIDER_TOKEN = 'configService';

export const pinoConfig: PinoConfig = {
  ...config.get<PinoConfig>('pino'),
  redact: {
    ...config.get('pino.redact'),
    paths:
      config.get<boolean>('pino.redact.enabled') && config.get<string[]>('pino.redact.paths')
        ? config.get('pino.redact.paths')
        : [],
  },
};

export const configProviders = [
  {
    provide: CONFIG_SERVICE_PROVIDER_TOKEN,
    useFactory: (): IConfig => config,
  },
  {
    provide: 'hostName',
    useFactory: (): string => getHost(),
  },
];
