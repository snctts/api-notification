export interface PinoRedactConfig {
  enabled: boolean;
  paths: string[];
  censor?: any;
}

export interface PinoConfig {
  enabled?: boolean;
  redact?: PinoRedactConfig;
}
