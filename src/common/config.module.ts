import {Module, Global} from '@nestjs/common';

import {configProviders} from './config.provider';

@Global()
@Module({
  imports: [],
  providers: [...configProviders],
  exports: [...configProviders],
})
export class CommonModule {}
