import {Module} from '@nestjs/common';
import {SendNotificationService} from './notification-firebase.service';
import {SendNotificationController} from './notification-firebase.controller';
import {FireBaseModule} from '../firebase/firebase.module';

@Module({
  imports: [FireBaseModule],
  controllers: [SendNotificationController],
  providers: [SendNotificationService],
})
export class SendNotificationModule {}
