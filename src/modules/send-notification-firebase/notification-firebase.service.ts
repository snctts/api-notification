import {ConflictException, Injectable, InternalServerErrorException} from '@nestjs/common';
import {Timeout} from '@nestjs/schedule';
import {debugLog, errorLog, infoLog} from '../../shared/logger';
import {FirebaseService} from '../firebase/firebase.service';
import {SendBatchNotificationInput, SendNotificationInput} from './notification-firebase.dto';

@Injectable()
export class SendNotificationService {
  private readonly notification_options = {
    priority: 'high',
    timeToLive: 60 * 60 * 24,
  };

  constructor(private firebaseService: FirebaseService) {}

  /*
  ping connection and send notification to firebase after run service 2000 ms
  check ping connection and send notification to firebase by endpoints
  */
  @Timeout('notifications', 2000)
  async pingPongService() {
    try {
      await this.firebaseService
        .getAdmin()
        .messaging()
        .sendToDevice(
          'Ping_Device_Id',
          {notification: {title: 'check connection', body: 'check connection'}},
          {
            priority: 'high',
            timeToLive: 60 * 60 * 24,
          },
        );
      infoLog('Connection success', 'Send notification success');
      return {
        message: 'Connection success',
      };
    } catch (err) {
      errorLog('Connection failed!', err.message);
      throw new InternalServerErrorException({
        message: 'Failed connection to firebase!',
        name: 'InternalServerError',
        error: {
          responseData: err.message,
        },
      });
    }
  }

  // send notification to firebase give one device
  // input SendNotificationInput (input required field)
  async sendNotification(body: SendNotificationInput) {
    const {registrationToken, message, data} = body;
    const additionalPayload = data;
    debugLog(`Entering send notification to device ${registrationToken}`, message);
    const options = this.notification_options;
    try {
      /* tslint:disable:no-string-literal */
      if (additionalPayload) message['data'] = additionalPayload;
      /* tslint:enable:no-string-literal */
      await this.firebaseService
        .getAdmin()
        .messaging()
        .sendToDevice(registrationToken, message, options);

      return {
        success: true,
        deviceId: registrationToken,
        message,
      };
    } catch (err) {
      errorLog('Send notification failed', err.message);
      throw new InternalServerErrorException({
        message: 'Failed connection to firebase!',
        name: 'InternalServerError',
        error: {
          responseData: err.message,
        },
      });
    }
  }

  // send batch notification to firebase give one device
  // input SendBatchNotificationInput (input required field)
  async sendBatchNotification(body: SendBatchNotificationInput) {
    const {registrationToken, message, data} = body;
    const additionalPayload = data;
    if (
      Array.isArray(registrationToken) !== true ||
      !registrationToken ||
      registrationToken.length > 100 ||
      message === '' ||
      !message ||
      Object.keys(message).length === 0
    ) {
      throw new ConflictException('Invalid body');
    } else {
      try {
        /* tslint:disable:no-string-literal */
        if (additionalPayload) message['data'] = additionalPayload;
        /* tslint:enable:no-string-literal */

        const notification = {
          title: message.notification?.title,
          body: message.notification?.body,
        };

        await this.firebaseService.getAdmin().messaging().sendMulticast({
          tokens: registrationToken,
          data: additionalPayload,
          notification,
        });
        return {
          success: true,
          deviceId: registrationToken,
          message,
        };
      } catch (err) {
        errorLog('Send notification failed', err.message);
        throw new InternalServerErrorException({
          message: 'Failed connection to firebase!',
          name: 'InternalServerError',
          error: {
            responseData: err.message,
          },
        });
      }
    }
  }
}
