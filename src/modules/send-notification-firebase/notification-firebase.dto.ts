import {IsNotEmpty, IsObject, IsOptional, IsString, IsArray} from 'class-validator';
import {ApiProperty} from '@nestjs/swagger';

export class NotificationInput {
  @IsOptional()
  @IsString()
  @ApiProperty({
    type: 'string',
    description: 'title notification',
  })
  title?: string;

  @IsString()
  @IsNotEmpty()
  @ApiProperty({
    type: 'string',
    description: 'body notification',
  })
  body: string;

  @IsOptional()
  @IsString()
  @ApiProperty({
    type: 'string',
    description: 'click_action notification',
  })
  click_action?: string;

  @IsOptional()
  @IsString()
  @ApiProperty({
    type: 'string',
    description: 'icon notification',
  })
  icon?: string;
}

export class MessageNotificationInput {
  @ApiProperty({
    type: NotificationInput,
    description: 'notification in message',
  })
  @IsNotEmpty()
  @IsObject()
  notification: NotificationInput;
}

export class SendNotificationInput {
  @ApiProperty({
    type: 'string',
    description: 'registrationToken',
  })
  @IsString()
  @IsNotEmpty()
  registrationToken: string;

  @ApiProperty({
    type: MessageNotificationInput,
    description: 'message',
  })
  @IsObject()
  @IsNotEmpty()
  message: any;

  @ApiProperty({
    type: 'object',
    description: 'data',
  })
  @IsOptional()
  data?: any;
}

export class SendBatchNotificationInput {
  @ApiProperty({
    type: 'string',
    isArray: true,
    description: 'list registrationToken',
  })
  @IsArray()
  @IsNotEmpty()
  registrationToken: string[];

  @ApiProperty({
    type: MessageNotificationInput,
    description: 'message',
  })
  @IsObject()
  @IsNotEmpty()
  message: any;

  @ApiProperty({
    type: 'object',
    description: 'data',
  })
  @IsOptional()
  data?: any;
}
