import {Body, Controller, Get, Post} from '@nestjs/common';
import {ApiBody, ApiOperation, ApiTags} from '@nestjs/swagger';
import {SendBatchNotificationInput, SendNotificationInput} from './notification-firebase.dto';
import {SendNotificationService} from './notification-firebase.service';

@ApiTags('firebase')
@Controller('firebase')
export class SendNotificationController {
  constructor(private sendNotificationService: SendNotificationService) {}

  @Post('')
  @ApiOperation({
    operationId: 'sendNotification',
  })
  @ApiBody({
    type: SendNotificationInput,
    description: 'sendNotification',
  })
  sendNotification(@Body() input: SendNotificationInput) {
    return this.sendNotificationService.sendNotification(input);
  }

  @Get('ping')
  @ApiOperation({
    operationId: 'pingConnection',
  })
  pingConnection() {
    return this.sendNotificationService.pingPongService();
  }

  @Post('batch')
  @ApiOperation({
    operationId: 'sendBatchNotification',
  })
  @ApiBody({
    type: SendBatchNotificationInput,
    description: 'SendBatchNotificationInput',
  })
  sendBatchNotification(@Body() input: SendBatchNotificationInput) {
    return this.sendNotificationService.sendBatchNotification(input);
  }
}
