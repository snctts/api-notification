import {INestApplication} from '@nestjs/common';
import {Test} from '@nestjs/testing';
import {MongoMemoryServer} from 'mongodb-memory-server';
import {AppModule} from '../../../app.module';
import * as Logger from '../../../shared/logger';
import {createRequestFunction, initTestApp} from '../../../shared/test.helpers';
import {SendNotificationInput, SendBatchNotificationInput} from '../notification-firebase.dto';

jest.setTimeout(30000);

describe('notification e2e', () => {
  let app: INestApplication;
  let db: MongoMemoryServer;
  let request: ReturnType<typeof createRequestFunction>;

  const sendNotification = '/firebase';
  const batchNotification = '/firebase/batch';

  const message = {
    notification: {
      title: 'Title notification',
      body: 'Body notification',
    },
  };

  const inputSendNotification: SendNotificationInput = {
    registrationToken: 'device Id',
    message,
  };

  const inputSendBatchNotification: SendBatchNotificationInput = {
    registrationToken: ['device Id', 'device Id 2'],
    message,
  };

  beforeAll(async () => {
    [app, db] = await initTestApp(() => {
      return Test.createTestingModule({
        imports: [AppModule],
        providers: [],
      });
    });

    request = createRequestFunction(app);
  });
  afterAll(async () => {
    jest.clearAllMocks();
    jest.restoreAllMocks();

    await Promise.all([app?.close(), db?.stop()]);
  });
  beforeEach(async () => {});

  describe('ping connection and send notification to firebase', () => {
    describe('Bad case', () => {
      it('request wrong path url', async () => {
        const res = await request('/api/ping', {
          expected: 404,
          method: 'post',
          body: {},
        });
        expect(res.body).toStrictEqual({
          errorResponse: 'Not Found',
          message: 'Cannot POST /api/ping',
          statusCode: 404,
          timestamp: expect.any(String),
        });
      });
    });
    describe('Happy case', () => {
      it('ping connection success', async () => {
        const res = await request('/firebase/ping', {
          expected: 200,
          method: 'get',
        });
        expect(res.body).toStrictEqual({
          message: 'Connection success',
        });
      });
    });
  });

  describe('call /firebase', () => {
    describe('Request failed', () => {
      it('Bad request validate dto', async () => {
        const res = await request(sendNotification, {
          expected: 400,
          method: 'post',
          body: {},
        });

        expect(res.body.message).toStrictEqual([
          'registrationToken should not be empty',
          'registrationToken must be a string',
          'message should not be empty',
          'message must be an object',
        ]);
      });

      it('Bad case', async () => {
        jest.spyOn(Logger, 'errorLog');
        const res = await request(sendNotification, {
          expected: 500,
          method: 'post',
          body: {
            registrationToken: 'device Id',
            message: {
              title: 'Title notification',
              body: 'Body notification',
            },
          },
        });

        expect(Logger.errorLog).toHaveBeenLastCalledWith(
          'Send notification failed',
          'Messaging payload contains an invalid "title" property. Valid properties are "data" and "notification".',
        );

        expect(res.body.message).toBe('Failed connection to firebase!');
      });

      describe('Happy Case', () => {
        it('send notification success', async () => {
          const res = await request(sendNotification, {
            expected: 201,
            method: 'post',
            body: inputSendNotification,
          });

          expect(res.body).toStrictEqual({
            success: true,
            deviceId: inputSendNotification.registrationToken,
            message: inputSendNotification.message,
          });
        });
      });
    });

    describe('call batch notification', () => {
      describe('Bad Case', () => {
        it('validation failed', async () => {
          const res = await request(batchNotification, {
            expected: 400,
            method: 'post',
            body: {},
          });

          expect(res.body.message).toStrictEqual([
            'registrationToken should not be empty',
            'registrationToken must be an array',
            'message should not be empty',
            'message must be an object',
          ]);
        });

        it('failed connection to firebase', async () => {
          jest.spyOn(Logger, 'errorLog');

          const res = await request(batchNotification, {
            expected: 500,
            method: 'post',
            body: {
              ...inputSendBatchNotification,
              registrationToken: [],
            },
          });
          expect(Logger.errorLog).toHaveBeenLastCalledWith(
            'Send notification failed',
            'tokens must be a non-empty array',
          );

          expect(res.body.message).toStrictEqual('Failed connection to firebase!');
        });
      });

      describe('Happy case', () => {
        it('send success', async () => {
          const res = await request(batchNotification, {
            expected: 201,
            method: 'post',
            body: inputSendBatchNotification,
          });

          expect(res.body).toStrictEqual({
            success: true,
            message: inputSendBatchNotification.message,
            deviceId: inputSendBatchNotification.registrationToken,
          });
        });
      });
    });
  });
});
