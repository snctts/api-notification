import {InternalServerErrorException} from '@nestjs/common';
import {Test} from '@nestjs/testing';
import * as Logger from '../../shared/logger';
import {FirebaseService} from '../firebase/firebase.service';
import {SendNotificationService} from './notification-firebase.service';

jest.setTimeout(30000);

describe('SendNotificationService', () => {
  let service: SendNotificationService;

  const mockFireBaseService = {
    getAdmin: jest.fn(),
  };

  beforeAll(async () => {
    const module = await Test.createTestingModule({
      imports: [],
      providers: [
        {
          provide: FirebaseService,
          useValue: mockFireBaseService,
        },
        SendNotificationService,
      ],
    }).compile();
    service = module.get<SendNotificationService>(SendNotificationService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });

  describe('ping pong connection', () => {
    beforeEach(() => {});
    afterEach(() => {
      jest.clearAllMocks();
    });

    it('ping pong connection failed', async () => {
      jest.spyOn(Logger, 'errorLog');

      mockFireBaseService.getAdmin;

      try {
        await service.pingPongService();
      } catch (err) {
        expect(err).toBeInstanceOf(InternalServerErrorException);
        expect(Logger.errorLog).toHaveBeenLastCalledWith('Connection failed!', expect.any(String));
      }
    });

    it('ping pong connection success', async () => {
      jest.spyOn(Logger, 'infoLog');

      mockFireBaseService.getAdmin.mockReturnValue({
        messaging: jest.fn().mockReturnValue({
          sendToDevice: jest.fn().mockReturnValue({
            success: true,
          }),
        }),
      });
      await service.pingPongService();

      expect(Logger.infoLog).toHaveBeenLastCalledWith('Connection success', expect.any(String));
    });
  });
});
