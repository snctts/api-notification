import {getConfigFirebase} from '../../common/config.provider';
import * as admin from 'firebase-admin';

const configFireBase = getConfigFirebase();

if (!admin.apps.length) {
  admin.initializeApp({
    credential: admin.credential.cert({
      clientEmail: configFireBase.firebaseClientEmail,
      privateKey: configFireBase.firebasePrivateKey.replace(/\\n/g, '/n'),
      projectId: configFireBase.firebaseProjectId,
    } as Partial<admin.ServiceAccount>),
    databaseURL: configFireBase.firebaseDatabaseUrl,
    storageBucket: configFireBase.firebaseStorageBuck,
  });
}

export class FirebaseService {
  getAdmin() {
    return admin;
  }
}
