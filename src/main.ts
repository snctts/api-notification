import {NestFactory} from '@nestjs/core';
import {AppModule} from './app.module';
import {getConfig, getHost, getPort} from './common/config.provider';
import {configureApp, initializeSwagger} from './shared/bootstrap';
import {infoLog} from './shared/logger';
import {config as dotenvConfig} from 'dotenv';
dotenvConfig();

const port = getPort();
const config = getConfig();

async function bootstrap() {
  const app = await NestFactory.create(AppModule);

  configureApp(app);

  await initializeSwagger(app);

  await app.listen(port, () => {
    infoLog('main', 'Start Listening with Port:', port);
  });
}

bootstrap()
  .then(() => {
    const hostname = getHost();
    infoLog(
      'main',
      `Docs available on http(s)://${hostname}${getConfig().get('service.docsBaseUrl')}`,
    );
  })
  .catch((error) => {
    infoLog('main', error, 'bootstrap starting error ');
  });
