import {schedule, danger, warn, markdown, fail} from 'danger';

schedule(async () => {
  // Setup
  const mr = danger.gitlab.mr;

  // Check if MR is BIG!
  const BIG_MR_THRESHOLD = 500;
  const linesOfCode = await danger.git.linesOfCode();
  if (linesOfCode > BIG_MR_THRESHOLD) {
    warn(`:exclamation: Big MR`);
    markdown(
      `> (${linesOfCode} (actual) > ${BIG_MR_THRESHOLD} (expected)) Merge Request size seems relatively large. If Merge Request contains multiple changes, split each into separate MR will helps faster, easier review.`,
    );
  }

  // Check for description
  if (mr.description.length < 10) {
    fail('This merge request needs a description.');
  }
});
